# Team Members

* Kyle Hagerman
* Karen Stengel
* Jared Thompson
* Tim Wells

# Idea 1
**Title: Normal Forms: An Introduction**  
**Objective:** To understand the methodology behind data normalization including how to implement the three forms and why they are used.  
**Description:** This tutorial will introduce the reader to the concept of normal forms. By the end of the tutorial, they should understand: 
when to use normal forms, how to create a database in normal form, and how to convert between the three normal forms. The purpose of this tutorial 
is to aid students in understanding and implementing normal forms. 
  
# Idea 2
**Title: Syntax + Basic Commands in SQL: A Beginner’s Guide**  
**Objective:** To learn basic syntax and commands for a working knowledge of SQL.  
**Description:** This guide will introduce SQL beginners to the language and how to use it, covering 
basic syntactic rules and key operations. Though this won’t cover everything, it will 
be a useful set of guidelines for novices to begin working in SQL without being overwhelmed by all of its advanced capabilities.
  
# Idea 3
**Title: Topics in Cartesian Products and a Refresher in Elementary Set Theory**  
**Objective:** To learn how to construct valid join and select queries based on set theory.  
**Description:** Hopefully the reader will form a basis of how the join and select features 
function to write queries. Also, we will provide a handy refresher to basic set theory including union, 
intersection, and the cartesian product in case the reader has forgotten. Examples will include a basic 
set example and a couple of join and select problems. This tutorial holds value because if a reader better 
understands the cartesian product, then they can write more efficient queries to find what they’re looking for. 
