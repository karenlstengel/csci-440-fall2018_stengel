SELECT name, grade FROM Highschooler
WHERE NOT EXISTS(SELECT * FROM (Highschooler AS Hs2 JOIN Friend ON Friend.ID1 = Hs2.ID) WHERE Hs2.grade = Highschooler.grade AND Friend.ID2 = Highschooler.ID)
AND NOT EXISTS(SELECT * FROM (Highschooler AS Hs2 JOIN Friend ON Friend.ID2 = Hs2.ID) WHERE Hs2.grade = Highschooler.grade AND Friend.ID1 = Highschooler.ID);
