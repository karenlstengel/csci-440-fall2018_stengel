--Find the name and grade of all students who are liked by more than one other student.

.read data.sql
.mode columns
.headers ON

--join highschooler on ID2 = ID with select ID2 and count where numlikes > 1
SELECT name, grade FROM Highschooler JOIN (SELECT ID2, count(ID2) AS count FROM Likes GROUP BY ID2)Likes ON Likes.ID2 = Highschooler.ID WHERE Likes.count > 1;
