.read data.sql
.mode column
.headers on

select name, grade
from (select Highschooler.name, Highschooler.grade, Highschooler.ID, count(*) 
	as NumberOfFriends
	from (Highschooler join Friend
		on Highschooler.ID = Friend.ID1
		or Highschooler.ID = Friend.ID2)
	group by Highschooler.ID)
where NumberOfFriends = (select max(NumberOfFriends)
				from (select Highschooler.name, Highschooler.grade, Highschooler.ID, count(*)
				as NumberOfFriends
				from (Highschooler join Friend
					on Highschooler.ID = Friend.ID1
					or Highschooler.ID = Friend.ID2)
				group by Highschooler.ID));

--Find the name and grade of the student(s) with the greatest number of friends.

