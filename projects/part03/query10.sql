.read data.sql

SELECT Hs1.name, Hs1.grade, "<--Likes-->" AS Likes, Hs2.name, Hs2.grade 
FROM((Likes JOIN Highschooler AS Hs1 ON Likes.ID1 = Hs1.ID)
	JOIN Highschooler AS Hs2 ON Likes.ID2 = Hs2.ID)
WHERE Hs1.grade - Hs2.grade >= 2;