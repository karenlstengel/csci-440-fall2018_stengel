--Find the difference between the number of students in the school and the number of different first names.

.read data.sql
.mode columns
.headers ON

SELECT (SELECT count(*) FROM Highschooler) - (SELECT count(distinct name) FROM Highschooler) AS difference;
