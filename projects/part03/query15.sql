--For each student A who likes a student B where the two are not friends, find if they have a friend C in common (who can introduce them!). For all such trios, return the name and grade of A, B, and C.
-- Received help from Keely Weisbeck

.read data.sql
.mode columns
.headers ON

SELECT A.name, A.grade, B.name, B.grade, C.name, C.grade 
FROM Likes, Highschooler A, Highschooler B, Highschooler C, Friend FA, Friend FB
WHERE A.ID = Likes.ID1 AND Likes.ID2 = B.ID and
	B.ID NOT IN (SELECT ID2 FROM Friend WHERE ID1 = A.ID) AND
	A.ID = FA.ID1 AND FA.ID2 = C.ID AND
	C.ID = FB.ID1 AND FB.ID2 = B.ID;
