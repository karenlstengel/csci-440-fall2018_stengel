.read data.sql

SELECT name, grade FROM Highschooler
WHERE EXISTS(SELECT * FROM Likes AS Likes1 WHERE Highschooler.ID = Likes1.ID1 
AND NOT EXISTS(SELECT * FROM Likes AS Likes2 WHERE Likes2.ID1 = Likes1.ID2))
OR EXISTS(SELECT * FROM Likes AS Likes1 WHERE Highschooler.ID = Likes1.ID2
AND NOT EXISTS(SELECT * FROM Likes AS Likes2 WHERE Likes2.ID1 = Likes1.ID2))
ORDER BY grade, name;