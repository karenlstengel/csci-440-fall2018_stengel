.read data.sql
.mode columns
.headers on

select name from Highschooler
where exists (select * from (Friend join Highschooler as Hs2 on Friend.ID1 = Hs2.ID)
	where Hs2.name = "Gabriel" and Friend.ID2 = Highschooler.ID)
or exists (select * from (Friend join Highschooler as Hs2 on Friend.ID2 = Hs2.ID)
	where Hs2.name = "Gabriel" and Friend.ID1 = Highschooler.ID);
