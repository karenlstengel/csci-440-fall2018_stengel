-- Find names and grades of students who only have friends in the same grade. Return the result sorted by grade, then by name within each grade.

.read data.sql
.mode columns
.headers ON

SELECT grade, name FROM Highschooler
WHERE NOT EXISTS (SELECT * FROM (Friend JOIN Highschooler AS Hs2 ON Friend.ID1 = Hs2.ID) WHERE Hs2.grade != Highschooler.grade AND Highschooler.ID = Friend.ID2)
AND NOT EXISTS (SELECT * FROM (Friend JOIN Highschooler AS Hs2 ON Friend.ID2 = Hs2.ID) WHERE Hs2.grade != Highschooler.grade AND Highschooler.ID = Friend.ID1) ORDER by grade asc, name;
