-- query 7: Find the number of students who are either friends with Cassandra
-- or are friends of friends of Cassandra. Do not count Cassandra, even though
--technically she is a friend of a friend.

.read data.sql
.mode columns
.headers ON

SELECT (SELECT count(ID1) FROM Friend WHERE Friend.ID2 = (SELECT ID FROM Highschooler WHERE Highschooler.name = "Cassandra")) +

(SELECT count(ID2) FROM Friend where Friend.ID1 = (SELECT ID FROM Highschooler where Highschooler.name = "Cassandra")) +

(SELECT count (ID1)
FROM Friend
WHERE (Friend.ID1 = (SELECT ID2
                        FROM Friend
                        WHERE Friend.ID1 = (SELECT ID
                                                FROM Highschooler
                                                WHERE Highschooler.name = "Cassandra"))
AND Friend.ID1 != (SELECT ID
                        FROM Highschooler
                        WHERE Highschooler.name = "Cassandra"))
OR (Friend.ID1 = (SELECT ID1
                        FROM Friend
                        WHERE Friend.ID2 = (SELECT ID
                                                FROM Highschooler
                                                WHERE Highschooler.name = "Cassandra"))
AND Friend.ID2 != (SELECT ID
                        FROM Highschooler
                        WHERE Highschooler.name = "Cassandra"))) +

(SELECT count (ID2)
FROM Friend
WHERE (Friend.ID2 = (SELECT ID1
                        FROM Friend
                        WHERE Friend.ID2 = (SELECT ID
                                                FROM Highschooler
                                                WHERE Highschooler.name = "Cassandra"))
AND Friend.ID2 != (SELECT ID
                        FROM Highschooler
                        WHERE Highschooler.name = "Cassandra"))
OR (Friend.ID2 = (SELECT ID2
                        FROM Friend
                        WHERE Friend.ID1 = (SELECT ID
                                                FROM Highschooler
                                                WHERE Highschooler.name = "Cassandra"))
AND Friend.ID1 != (SELECT ID
                        FROM Highschooler
                        WHERE Highschooler.name = "Cassandra"))) as "Friends of Cassandra";
